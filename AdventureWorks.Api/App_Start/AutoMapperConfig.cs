﻿using AdventureWorks.Api.Models;
using AutoMapper;
using Orbus.AdventureWorks.DataAccess.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdventureWorks.Api.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Product, ProductViewModel>().ReverseMap();
                cfg.CreateMap<ProductCategory, ProductCategoryViewModel>().ReverseMap();
                cfg.CreateMap<ProductSubcategory, ProductSubcategoryViewModel>().ReverseMap();
            });
        }
    }
}