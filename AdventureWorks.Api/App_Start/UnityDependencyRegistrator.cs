﻿using AdventureWorks.Api.Validators.Product;
using Microsoft.Practices.Unity;
using Orbus.AdventureWorks.DataAccess;
using System.Configuration;
using System.Web.Http;

namespace AdventureWorks.Api.App_Start
{
    public class UnityDependencyRegistrator
    {
        public static void Register(HttpConfiguration config)
        {
            UnityContainer unitycontainer = new UnityContainer();
            unitycontainer.RegisterType<IAdventureWorksContext>(new InjectionFactory((container, type, name) =>
            {
                return new AdventureWorksContext(ConfigurationManager.ConnectionStrings["AdventureWorks"].ConnectionString);
            }));

            unitycontainer.RegisterType<IProductValidator, ProductValidator>();

            config.DependencyResolver = new UnityResolver(unitycontainer);
        }
    }
}