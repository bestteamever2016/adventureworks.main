﻿using AdventureWorks.Api.App_Start;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Serialization;
using Orbus.AdventureWorks.DataAccess;
using Orbus.AdventureWorks.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AdventureWorks.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Dependency resolver registration
            UnityDependencyRegistrator.Register(config);

            // AutoMapper
            AutoMapperConfig.RegisterMappings();

            // Camel casing
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
