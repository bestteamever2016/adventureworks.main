﻿using System.Web.Http;
using Orbus.AdventureWorks.DataAccess;

namespace AdventureWorks.Api.Controllers.Api
{
    public abstract class BaseController : ApiController
    {
        protected IAdventureWorksContext Unit;

        protected BaseController(IAdventureWorksContext unit)
        {
            Unit = unit;
        }
    }
}
