﻿using System.Collections.Generic;
using System.Web.Http;
using Orbus.AdventureWorks.DataAccess;
using System.Threading.Tasks;
using AdventureWorks.Api.Models;
using AutoMapper;
using AdventureWorks.Api.Interceptors;

namespace AdventureWorks.Api.Controllers.Api
{
    [AuthorizedOnly]
    public class ProductCategoryController : BaseController
    {
        public ProductCategoryController(IAdventureWorksContext unit) : base(unit)
        {
        }

        [HttpGet]
        public async Task<ProductCategoryViewModel> Get(int id)
        {
            var result = await Unit.ProductCategoryRepository.GetProductCategoryById(id);
            return Mapper.Map<ProductCategoryViewModel>(result);
        }

        [HttpGet]
        public async Task<IEnumerable<ProductCategoryViewModel>> Get()
        {
            var result = await Unit.ProductCategoryRepository.GetProductCategories();
            return Mapper.Map<List<ProductCategoryViewModel>>(result);
        }
    }
}
