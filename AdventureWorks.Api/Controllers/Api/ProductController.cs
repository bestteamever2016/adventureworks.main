﻿using AutoMapper;
using System.Collections.Generic;
using System.Web.Http;
using System.Threading.Tasks;
using Orbus.AdventureWorks.DataAccess;
using Orbus.AdventureWorks.DataAccess.Entity;
using AdventureWorks.Api.Models;
using AdventureWorks.Api.Interceptors;
using System.Net.Http;
using System.Net;

namespace AdventureWorks.Api.Controllers.Api
{
    [AuthorizedOnly]
    public class ProductController : BaseController
    {
        public ProductController(IAdventureWorksContext unit) : base(unit)
        {
        }

        [HttpGet]
        public async Task<IEnumerable<Product>> Get()
        {
            return await Unit.ProductRepository.GetProducts();
        }

        [HttpGet]
        public async Task<ProductViewModel> Get(int id)
        {
            var product = await Unit.ProductRepository.GetProductById(id);
            return Mapper.Map<ProductViewModel>(product);
        }

        [HttpGet]
        public async Task<IEnumerable<ProductViewModel>> GetBySubCategory(int subCategoryId)
        {
            var products = await Unit.ProductRepository.GetProductsByProductSubCategoryId(subCategoryId);
            return Mapper.Map<List<ProductViewModel>>(products);
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Delete(ProductViewModel product)
        {
            var isValidProductKey = await Unit.ProductRepository.IsValidProductKey(product.Key);
            if (ModelState.IsValid && isValidProductKey)
            {
                await Unit.ProductRepository.CreateProduct(Mapper.Map<Product>(product));
                return Request.CreateResponse(HttpStatusCode.OK, "Product created");
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, "You passed wrong data.");
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(int id, [FromBody]ProductViewModel product)
        {
            var isValidProductKey = await Unit.ProductRepository.IsValidProductKey(product.Key);
            if (ModelState.IsValid && isValidProductKey)
            {
                await Unit.ProductRepository.UpdateProduct(Mapper.Map<Product>(product));
                return Request.CreateResponse(HttpStatusCode.OK, "Product updated");
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, "You passed wrong data.");
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> Delete(int id)
        {
            await Unit.ProductRepository.DeleteProduct(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
        }
    }
}
