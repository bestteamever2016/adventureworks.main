﻿using AutoMapper;
using AdventureWorks.Api.Interceptors;
using AdventureWorks.Api.Models;
using Orbus.AdventureWorks.DataAccess;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace AdventureWorks.Api.Controllers.Api
{
    [AuthorizedOnly]
    public class ProductSubCategoryController : BaseController
    {
        public ProductSubCategoryController(IAdventureWorksContext unit) : base(unit)
        {
        }

        [HttpGet]
        public async Task<IEnumerable<ProductCategoryViewModel>> Get()
        {
            var categories = await Unit.ProductSubCategoryRepository.GetProductSubcategories();
            return Mapper.Map<List<ProductCategoryViewModel>>(categories);
        }

        [HttpGet]
        public async Task<ProductSubcategoryViewModel> Get(int id)
        {
            var subCategory = await Unit.ProductSubCategoryRepository.GetProductSubcategoryById(id);
            return Mapper.Map<ProductSubcategoryViewModel>(subCategory);
        }

        [HttpGet]
        public async Task<IEnumerable<ProductSubcategoryViewModel>> GetByCategoryId(int categoryId)
        {
            var subcategories = await Unit.ProductSubCategoryRepository.GetProductSubcategoriesByProductCategoryId(categoryId);
            return Mapper.Map<List<ProductSubcategoryViewModel>>(subcategories);
        }
    }
}
