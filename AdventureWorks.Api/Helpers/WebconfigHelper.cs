﻿using System.Configuration;

namespace AdventureWorks.Api.Helpers
{
    public class WebconfigHelper
    {
        public static string AuthorizationParameter => GetByKey("authorizationParameter");

        public static string AuthorizationScheme => GetByKey("authorizationScheme");

        public static string GetByKey(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}