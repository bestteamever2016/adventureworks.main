﻿using AdventureWorks.Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace AdventureWorks.Api.Interceptors
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class AuthorizedOnlyAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var authorizeHeader = actionContext.Request.Headers.Authorization;
            
            return authorizeHeader != null &&
                   authorizeHeader.Parameter == WebconfigHelper.AuthorizationParameter &&
                   authorizeHeader.Scheme == WebconfigHelper.AuthorizationScheme;
        }
    }
}