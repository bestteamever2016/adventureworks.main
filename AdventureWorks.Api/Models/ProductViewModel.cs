﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdventureWorks.Api.Models
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Key { get; set; }
        public int? ProductSubcategoryId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public short? StockLevel { get; set; }
        public decimal? Price { get; set; }
    }
}