﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventureWorks.Api.Validators
{
    public interface IValidator
    {
        bool IsValid { get; }
        void Validate();
        string Message { get; }
    }
}
