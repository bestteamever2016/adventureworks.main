﻿using AdventureWorks.Api.Models;
using Orbus.AdventureWorks.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdventureWorks.Api.Validators.Product
{ 
    public interface IProductValidator
    {
        bool IsValid { get; }
        void Validate();
        void AttachContext(ProductViewModel productVm, IAdventureWorksContext ctx);
        IEnumerable<string> Messages { get; }
    }
}