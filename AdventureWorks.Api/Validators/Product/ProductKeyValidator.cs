﻿using AdventureWorks.Api.Models;
using Microsoft.Practices.Unity;
using Orbus.AdventureWorks.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdventureWorks.Api.Validators.Product
{
    public class ProductKeyValidator : ValidatorBase<ProductViewModel>
    {
        private IAdventureWorksContext Unit { get; }
         
        public ProductKeyValidator(ProductViewModel context, IAdventureWorksContext ctx) : base(context)
        {
            Unit = ctx;
        }

        public override bool IsValid {
            get {
                return Unit.ProductRepository.IsValidProductKey(Context.Key).Result;
            }
        }

        public override string Message => "Product Key is not valid";




    }
}