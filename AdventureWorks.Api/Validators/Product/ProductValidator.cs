﻿using AdventureWorks.Api.Models;
using Orbus.AdventureWorks.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdventureWorks.Api.Validators.Product
{
    public class ProductValidator : IProductValidator
    {
        private List<IValidator> validators;
        public void AttachContext(ProductViewModel productVm, IAdventureWorksContext ctx)
        {
            validators = new List<IValidator> {
                new ProductKeyValidator(productVm, ctx)
            };
        }

        public bool IsValid
        {
            get
            {
                return validators.All(val => val.IsValid);
            }
        }

        public IEnumerable<string> Messages {
            get
            {
                return validators.Where(v => !v.IsValid).Select(v => v.Message);
            }
        }

        public void Validate()
        {
            foreach (var validation in validators)
            {
                validation.Validate();
            }
        }
    }
}