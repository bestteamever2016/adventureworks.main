﻿using System;

namespace AdventureWorks.Api.Validators
{
    public class ValidationException : Exception
    {
        public ValidationException(string message, params object[] args)
            : base(String.Format(message, args))
        {
        }
    }
}