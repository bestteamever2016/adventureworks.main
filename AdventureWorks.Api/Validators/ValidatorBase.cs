﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdventureWorks.Api.Validators
{
    public abstract class ValidatorBase<T> : IValidator where T : class
    {
        protected T Context { get; private set; }

        protected ValidatorBase(T context)
        {
            Context = context;
        }

        public void Validate()
        {
            if (!IsValid)
            {
                throw new ValidationException(Message);
            }
        }

        public abstract bool IsValid { get; }
        public abstract string Message { get; }
    }
}