﻿using Orbus.AdventureWorks.DataAccess.Repositories;

namespace AdventureWorks.Data.Unit
{
    public interface IUnitOfWork
    {
        IProductRepository ProductRepository { get; }
        IProductCategoryRepository ProductCategoryRepository { get; }
        IProductSubCategoryRepository ProductSubCategoryRepository { get; }
        void Commit();
    }
}
