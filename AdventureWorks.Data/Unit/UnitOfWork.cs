﻿using System;
using Orbus.AdventureWorks.DataAccess;
using Orbus.AdventureWorks.DataAccess.Repositories;

namespace AdventureWorks.Data.Unit
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool _disposed;
        private readonly AdventureWorksContext _adventureWorksContext;

        public IProductRepository ProductRepository => _adventureWorksContext.ProductRepository;
        public IProductCategoryRepository ProductCategoryRepository => _adventureWorksContext.ProductCategoryRepository;
        public IProductSubCategoryRepository ProductSubCategoryRepository => _adventureWorksContext.ProductSubCategoryRepository;

        public UnitOfWork(string connectionString)
        {
            _adventureWorksContext = new AdventureWorksContext(connectionString);
        }

        public void Commit()
        {
            //_adventureWorksContext.SaveChanges();
        }

        private void Dispose(bool disposing)
        {

            //if (!_disposed)
            //{
            //    if (disposing)
            //    {
            //        _adventureWorksContext.Dispose();
            //    }
            //}
            //_disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        //--------------------------------
        //      REPOSITORIES
        //--------------------------------
        //private IDictionaryRepository _dictionaryRepository;
        //public IDictionaryRepository DictionaryRepository
        //{
        //    get
        //    {
        //        if (_dictionaryRepository == null) _dictionaryRepository = new DictionaryRepository(_adventureWorksContext);
        //        return _dictionaryRepository;
        //    }
        //}
        
    }
}
