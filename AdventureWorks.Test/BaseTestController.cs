﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbus.AdventureWorks.DataAccess;

namespace AdventureWorks.Test
{
    [TestClass]
    public class BaseTestController
    {
        protected IAdventureWorksContext Unit { get; }
        public BaseTestController()
        {
            Unit = new AdventureWorksContext("Data Source=.\\SQLEXPRESS;Initial Catalog=AdventureWorksDW2008R2;Integrated Security=True");
        }
    }
}
