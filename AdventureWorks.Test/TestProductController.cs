﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdventureWorks.Api.Controllers.Api;
using System.Threading.Tasks;
using AdventureWorks.Api.Models;
using System.Collections.Generic;
using AdventureWorks.Api.App_Start;

namespace AdventureWorks.Test
{
    [TestClass]
    public class TestProductController : BaseTestController
    {
        [ClassInitialize]
        public static void Init(TestContext context)
        {
            AutoMapperConfig.RegisterMappings();
        }

        [TestMethod]
        public async Task ControllerAndDbContextShouldReturnSameAmountOfRecords()
        {
            var controller = new ProductController(Unit);
            IEnumerable<ProductViewModel> apiproducts = controller.GetBySubCategory(2).Result;
            var dbproducts = await Unit.ProductRepository.GetProductsByProductSubCategoryId(2);
            Assert.AreEqual((((List<ProductViewModel>)apiproducts).Count), dbproducts.Count);
        }

        [TestMethod]
        public async Task ControllerAndDbContextShouldReturnSameRecord()
        {
            var controller = new ProductController(Unit);
            ProductViewModel apiproduct = controller.Get(2).Result;
            var dbproduct = await Unit.ProductRepository.GetProductById(2);
            Assert.AreEqual(apiproduct.Id, dbproduct.Id);
            Assert.AreEqual(apiproduct.Name, dbproduct.Name);
            Assert.AreEqual(apiproduct.Key, dbproduct.Key);
        }
    }
}
