var dependencies = {
    js: [
        './libs_bower/jquery/dist/jquery.js',
        './libs_bower/angular/angular.js',
        './libs_bower/angular-ui-router/release/angular-ui-router.js',
        './libs_bower/bootstrap/dist/js/bootstrap.js',
        './libs_bower/lodash/dist/lodash.js',
        './libs_bower/angular-local-storage/dist/angular-local-storage.js',

        './libs_custom/angular-sanitize.js',
        './libs_custom/ui-bootstrap-custom-tpls-2.5.0.js',
        './libs_custom/dirPaginate.js',
        './libs_custom/select.js'
    ],
    css: [
        './libs_bower/html5-boilerplate/dist/css/main.css',
        './libs_bower/html5-boilerplate/dist/css/normalize.css',
        './libs_bower/bootstrap/dist/css/bootstrap.css',
        './libs_custom/select.css',
        './libs_custom/selectize.default.css'
    ],
    appScripts: function () {
        return [
            gulpconf.folders.dev.js + '/polyfills.js',
            gulpconf.folders.dev.app + '/external.js',
            gulpconf.folders.dev.app + '/app.js',
            gulpconf.folders.dev.app + '/constants.js',
            gulpconf.folders.dev.app + '/states.js',
            gulpconf.folders.dev.app + '/storage.js',
            gulpconf.folders.dev.app + '/factories/route.js',
            gulpconf.files.dev.app.factories,
            gulpconf.files.dev.app.directives,
            gulpconf.files.dev.app.components,
            gulpconf.files.dev.js
        ];
    }
}

var devMainFolder = 'www';
var distMainFolder = 'www/dist';

var gulpconf = {
    folders: {
        dev: {
            js: devMainFolder + '/js',
            css: devMainFolder + '/css',
            app: devMainFolder + '/app',
            libjs: devMainFolder + '/libs/js', 
            libcss: devMainFolder + '/libs/css'
        },
        dist: {
            js: distMainFolder + '/js',
            css: distMainFolder + '/css'
        }
    },
    files: {
        dev: {
            scss : devMainFolder + '/css/*.scss',
            css  : devMainFolder + '/css/*.css',
            js: devMainFolder + '/js/*.js',
            app: {
                root: devMainFolder + '/app/*.js',
                components: devMainFolder + '/app/component/**/*.js',
                directives: devMainFolder + '/app/directive/**/*.js',
                factories: devMainFolder + '/app/factory/**/*.js'
            }
        }
    },
    getLibsStyles: function () {
        return dependencies.css;
    },
    getLibsScriptsOrdered: function () {
        return dependencies.js;
    },
    getScriptsOrdered: dependencies.appScripts
}
module.exports = gulpconf;