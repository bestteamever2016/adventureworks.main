module.exports = function (gulp, plugins, settings) {
    return function () {
    	//if(settings.getLibsStyles()){
    		gulp.src(settings.getLibsStyles())
	        	.pipe(plugins.debug({title: 'debug:'}))
	            .pipe(gulp.dest(settings.folders.dev.libcss));
    	//}

    	//if(settings.getLibsScriptsOrdered()){
    		gulp.src(settings.getLibsScriptsOrdered())
	        	.pipe(plugins.debug({title: 'debug:'}))
	            .pipe(gulp.dest(settings.folders.dev.libjs));
    	//}
    };
};