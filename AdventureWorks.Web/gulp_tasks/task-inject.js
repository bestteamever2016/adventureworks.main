//'./www/index.html'
module.exports = {
    devInject: function (gulp, plugins, settings) {
        var all_files = settings.getLibsScriptsOrdered().map(function (path) {
                    return path.replace(path.substr(0, path.lastIndexOf('/')), settings.folders.dev.libjs)
                })
                .concat(settings.getScriptsOrdered())
                .concat([settings.folders.dev.libcss + '/*.css'])
                .concat([settings.files.dev.css]);
        return function () {
            gulp.src('./www/index.html')
                .pipe(plugins.logger({
                    before: 'Injecting production files into index.html...',
                    after: 'Injecting production files into index.html is completed!'
                }))
                .pipe(plugins.inject(gulp.src(all_files, { read: false }), { ignorePath: '/www' }))
                .pipe(plugins.debug({ title: 'file-info:' }))
                .pipe(gulp.dest('./www/'));
        };
    },
    distInject: function (gulp, plugins, settings) {
        var all_files = [
            './www/dist/js/lib.min.js',
            './www/dist/js/app.min.js',
            './www/dist/css/lib.min.css',
            './www/dist/css/app.min.css'
        ];
        return function () {
            gulp.src('./www/index.html')
                .pipe(plugins.logger({
                    before: 'Injecting production files into index.html...',
                    after: 'Injecting production files into index.html is completed!'
                }))
                .pipe(plugins.inject(gulp.src(all_files, { read: false }), { ignorePath: '/www' }))
                .pipe(plugins.debug({ title: 'file-info:' }))
                .pipe(gulp.dest('./www/'));
        };
    }
};