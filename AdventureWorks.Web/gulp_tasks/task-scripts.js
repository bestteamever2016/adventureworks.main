module.exports = {
    libJs: function (gulp, plugins, settings) {
        return function () {
            var libScripts = settings.getLibsScriptsOrdered().map(function (path) {
                return path.replace(path.substr(0, path.lastIndexOf('/')), settings.folders.dev.libjs)
            });

            gulp.src(libScripts)
                .pipe(plugins.logger({
                    before: 'Minifying and concating of js files has started...',
                    after: 'Minifying and concating of js files is completed!'
                }))
                .pipe(plugins.concat('lib.min.js'))
                .pipe(plugins.uglify().on('error', function (e) {
                    console.log(e);
                }))
                .pipe(gulp.dest(settings.folders.dist.js));
        };
    },
    appJs: function (gulp, plugins, settings) {
        return function () {
            gulp.src(settings.getScriptsOrdered())
                .pipe(plugins.logger({
                    before: 'Minifying and concating of js files has started...',
                    after: 'Minifying and concating of js files is completed!'
                }))
                .pipe(plugins.concat('app.min.js'))
                .pipe(plugins.uglify().on('error', function (e) {
                    console.log(e);
                }))
                .pipe(gulp.dest(settings.folders.dist.js));
        };
    }
};