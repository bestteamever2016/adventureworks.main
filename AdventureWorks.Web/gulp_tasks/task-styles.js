var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};
var autoprefixerOptions = {
    browsers: [
        'last 2 versions',
        'Firefox ESR',
        '> 5%'
    ]
};

module.exports = {
    sass: function (gulp, plugins, settings) {
        return function () {
            gulp.src(settings.files.dev.scss)
                .pipe(plugins.logger({
                    before: 'Compiling of sass has started...',
                    after: 'Compiling of sass is completed!'
                }))
                .pipe(plugins.debug({ title: 'file-info:' }))
                .pipe(plugins.sass(sassOptions).on('error', plugins.sass.logError))
                .pipe(gulp.dest(settings.folders.dev.css));

            if (plugins.argv.prod) {
                console.log('Passed parameter "--prod"');
                cssProduction(gulp, plugins, settings, settings.files.dev.css, 'lib.min.css');
            }
        };
    },
    appCss: function (gulp, plugins, settings) {
        return function () {
            cssProduction(gulp, plugins, settings, settings.files.dev.css, 'app.min.css');
        };
    },
    libCss: function (gulp, plugins, settings) {
        return function () {
            gulp.src(settings.getLibsStyles())
                .pipe(plugins.logger({
                    before: 'Concating of css files...',
                    after: 'Concating of css files is completed!'
                }))
                .pipe(plugins.debug({ title: 'file-info:' }))
                .pipe(plugins.cleanCss({ compatibility: 'ie8' }))
                .pipe(plugins.autoprefixer(autoprefixerOptions))
                .pipe(plugins.concat('lib.min.css'))
                .pipe(gulp.dest(settings.folders.dist.css));
        };
    }
};

function cssProduction(gulp, plugins, settings, srcFiles, destFilename) {
    gulp.src(srcFiles)
        .pipe(plugins.logger({
            before: 'Starting minifying and concating of css files...',
            after: 'Minifying and concating of css files is completed!'
        }))
        .pipe(plugins.debug({ title: 'file-info:' }))
        .pipe(plugins.cleanCss({ compatibility: 'ie8' }))
        .pipe(plugins.autoprefixer(autoprefixerOptions))
        .pipe(plugins.concat(destFilename))
        .pipe(gulp.dest(settings.folders.dist.css));
}