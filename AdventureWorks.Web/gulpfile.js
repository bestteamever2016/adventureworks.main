/// <binding />
var gulp = require('gulp');
var argv = require('yargs').argv;
var runSync = require('run-sequence');
var cleanCss = require('gulp-clean-css');
var config = require('./gulp.config.js');
var plugins = require('gulp-load-plugins')();
var rename = require("gulp-rename");

plugins.argv = argv;
plugins.cleanCss = cleanCss;

gulp.task('copy-libs', getTask('task-copy'));
gulp.task('sass', getTask('task-styles', 'sass'));
gulp.task('cssApp', getTask('task-styles', 'appCss'));
gulp.task('cssLib', getTask('task-styles', 'libCss'));
gulp.task('jsLib', getTask('task-scripts', 'libJs'));
gulp.task('jsApp', getTask('task-scripts', 'appJs'));

gulp.task('injectDev', getTask('task-inject', 'devInject'));
gulp.task('injectDist', getTask('task-inject', 'distInject'));


function getTask(taskFile, taskName) {
    return taskName ? require('./gulp_tasks/' + taskFile)[taskName](gulp, plugins, config)
                    : require('./gulp_tasks/' + taskFile)(gulp, plugins, config);
}