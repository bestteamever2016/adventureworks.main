'use strict';

angular.module('AdventureWorks.Factories', []);
angular.module('AdventureWorks.Directives', []);
angular.module('AdventureWorks.Filters', []);

angular.module('AdventureWorks.Controllers', [
    'AdventureWorks.Factories',
    'AdventureWorks.Directives',
    'AdventureWorks.Filters'
]);

angular.module('AdventureWorks', [
	'ui.router',
	'ui.select',
	'ui.bootstrap',
	'ngSanitize',
	'angularUtils.directives.dirPagination',
	'ExternalLibraries',
	'LocalStorageModule',
	'AdventureWorks.Controllers'
]);
