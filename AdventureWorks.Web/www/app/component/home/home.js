(function () {
    'use strict';

    angular.module('AdventureWorks.Controllers').controller('HomeController', ['api', '$scope', '$q', '$timeout', HomeController]);

    function HomeController(api, $scope, $q, $timeout) {
        var vm = this;
        vm.message = "HomeController";
    }
}());