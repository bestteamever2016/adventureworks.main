(function () {
    'use strict';

    angular.module('AdventureWorks.Controllers').controller('LayoutController', [LayoutController]);

    function LayoutController() {
        var vm = this;
        vm.message = "hello!";
    }
}());