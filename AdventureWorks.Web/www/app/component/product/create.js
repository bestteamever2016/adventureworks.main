(function() {
    'use strict';

    angular.module('AdventureWorks.Controllers').controller('ProductCreateController', ['api', '$timeout', '$state', '$scope', 'category', ProductCreateController]);

    function ProductCreateController(api, $timeout, $state, $scope, category) {
        var vm = this;

        vm.product = {};
        vm.requestStatus = '';

        vm.submitForm = function() {
            api.post('api/product/', vm.product).then(function(response) {
                vm.requestStatus = response.data;
                vm.requestStatusClass = 'success';
                console.log(response);
            }, function(error) {
                vm.requestStatus = error.status + ': ' + error.data;
                vm.requestStatusClass = 'error';
                console.log(error);
            });
        }

        vm.productCategories = [];
        vm.productSubCategories = [];

        vm.selectedCategory = {};
        vm.selectedSubCategory = {};

        initWatchers();

        vm.clearSelectedCategory = function() {
            vm.selectedCategory = {};
            vm.clearSelectedSubCategory();
        }
        vm.clearSelectedSubCategory = function() {
            vm.selectedSubCategory = {};
        }
        
        category.getCategories().then(function(categories) {
            vm.productCategories = categories;
        });

        function initWatchers() {
            $scope.$watch('ctrl.selectedCategory', function() {
                if (vm.selectedCategory.id) {
                    vm.clearSelectedSubCategory();
                    category.getSubCategories(vm.selectedCategory.id).then(function(subCategories) {
                        vm.productSubCategories = subCategories;
                    });
                }
            });

            $scope.$watch('ctrl.selectedSubCategory', function() {
                if (vm.selectedSubCategory.id) {
                    vm.product.productSubcategoryId = vm.selectedSubCategory.id;
                } else {
                    vm.product.productSubcategoryId = undefined;
                }

            });
        }
    }
}());