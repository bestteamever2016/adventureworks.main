(function() {
    'use strict';

    angular.module('AdventureWorks.Controllers').controller('ProductController', ['api', '$timeout', '$scope', '$uibModal', 'category', ProductController]);

    function ProductController(api, $timeout, $scope, $uibModal, category) {
        var vm = this;
        vm.message = 'hello!';

        vm.requestStatus = '';
        vm.products = [];
        vm.productCategories = [];
        vm.productSubCategories = [];

        vm.selectedCategory = {};
        vm.selectedSubCategory = {};

        initWatchers();

        vm.clearSelectedCategory = function() {
            vm.selectedCategory = {};
            vm.clearSelectedSubCategory();
        }
        vm.clearSelectedSubCategory = function() {
            vm.selectedSubCategory = {};
            vm.products.length = 0;
        }
        
        category.getCategories().then(function(categories) {
            vm.productCategories = categories;
        });


        vm.deleteProduct = function(product) {
            $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: function($scope, $uibModalInstance) {
                    $scope.product = product;
                    $scope.submitDeleting = function() {
                        api.delete('api/product/' + product.id).then(function(result) {
                            console.log(result);
                            $uibModalInstance.close();
                            vm.requestStatus = result.data;
                            vm.requestStatusClass = 'success';
                        }, function(error) {
                            console.log(error);
                        });
                    };
                    $scope.cancelDeleting = function() {
                        $uibModalInstance.close();
                    };
                }
            });
        }

        function initWatchers() {
            $scope.$watch('ctrl.selectedCategory', function() {
                if (vm.selectedCategory.id) {
                    vm.clearSelectedSubCategory();
                    category.getSubCategories(vm.selectedCategory.id).then(function(subCategories) {
                        vm.productSubCategories = subCategories;
                    });
                }
            });

            $scope.$watch('ctrl.selectedSubCategory', function() {
                if (vm.selectedSubCategory.id) {
                    api.get('api/product?subCategoryId=' + vm.selectedSubCategory.id).then(function(response) {
                        vm.products = response.data;
                    });
                }
            });
        }
        // Helpers 
        vm.table = {
            orderColumn: function(keyName) {
                vm.table.sortKey = keyName;
                vm.table.reverse = !vm.table.reverse;
            },
            showItemsCount: [
                { name: 'Show 10 items', value: 10 },
                { name: 'Show 20 items', value: 20 },
                { name: 'Show 50 items', value: 50 }
            ]
        };
        vm.table.showItemsCountSelected = vm.table.showItemsCount[0];
    }
}());