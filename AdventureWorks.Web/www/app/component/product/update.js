(function() {
    'use strict';

    angular.module('AdventureWorks.Controllers').controller('ProductUpdateController', ['api', '$timeout', '$scope', '$state', '$stateParams', 'category', ProductUpdateController]);

    function ProductUpdateController(api, $timeout, $scope, $state, $stateParams, category) {
        var vm = this;
        var productId = +$stateParams.id;

        vm.product = {};
        vm.requestStatus = '';

        api.get('api/product/' + productId).then(function(response) {
            vm.product = response.data;
            if(vm.product.productSubcategoryId !== null){
                api.get('api/productsubcategory/' + vm.product.productSubcategoryId).then(function(subcategory) {
                    if(subcategory.data.productCategoryId){
                        api.get('api/productcategory/' + subcategory.data.productCategoryId).then(function(category) {
                            vm.selectedCategory = category.data;
                            $timeout(function(){
                                vm.selectedSubCategory = subcategory.data;
                            }, 50)
                        });
                    }
                });
            }
        });

        vm.submitForm = function() {
            api.put('api/product/' + vm.product.id, vm.product).then(function(response) {
                console.log(response);
                vm.requestStatus = response.data;
                vm.requestStatusClass = 'success';
            }, function(error) {
                vm.requestStatus = error.status + ': ' + error.data;
                vm.requestStatusClass = 'error';
                console.log(error);
            });
        }


        vm.productCategories = [];
        vm.productSubCategories = [];

        vm.selectedCategory = {};
        vm.selectedSubCategory = {};

        initWatchers();

        vm.clearSelectedCategory = function() {
            vm.selectedCategory = {};
            vm.clearSelectedSubCategory();
        }
        vm.clearSelectedSubCategory = function() {
            vm.selectedSubCategory = {};
        }

        category.getCategories().then(function(categories) {
            vm.productCategories = categories;
        });

        function initWatchers() {
            $scope.$watch('ctrl.selectedCategory', function() {
                if (vm.selectedCategory.id) {
                    vm.clearSelectedSubCategory();
                    category.getSubCategories(vm.selectedCategory.id).then(function(subCategories) {
                        vm.productSubCategories = subCategories;
                    });
                }
            });

            $scope.$watch('ctrl.selectedSubCategory', function() {
                if (vm.selectedSubCategory.id) {
                    vm.product.productSubcategoryId = vm.selectedSubCategory.id;
                } else {
                    vm.product.productSubcategoryId = undefined;
                }

            });
        }


    }
}());