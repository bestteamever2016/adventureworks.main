angular.module('AdventureWorks')
.constant('constants', {
    apiServerOrigin: 'http://localhost:12500/',
    apiServerUrl: 'http://localhost:12500/api/v1/',
    lsKeys: {
        productCategories: 'productCategories',
        productSubCategories: 'productSubCategories_fromCategoryId='
    },
    customEvents: {
    }
});