angular.module('ExternalLibraries', [])
.factory('lodash', ['$window', function ($window) {
    return _;
}]);
/*
    other external libraries 
    will be wrapped in factories and described here
*/