(function() {
    'use strict';

    angular.module('AdventureWorks.Factories').factory('api', ['$http', '$q', 'constants', api]);

    function api($http, $q, constants) {
        
        var _headers = getDefaultHeaders();

        return {
            get: httpGet,
            post: httpPost,
            put: httpPut,
            delete: httpDelete
        }

        function httpGet(url, headers, options) {
            var deferred = $q.defer();

            var request = {
                url: constants.apiServerOrigin + url,
                method: 'GET',
                headers: angular.merge(_headers.authorization, headers)
            }

            $http(request).then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function httpPost(url, data, headers, options) {
            var deferred = $q.defer();

            var request = {
                url: constants.apiServerOrigin + url,
                method: 'POST',
                headers: angular.merge(_headers.authorization, _headers.contentType, headers),
                data: data
            }

            $http(request).then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function httpPut(url, data, headers, options) {
            var deferred = $q.defer();

            var request = {
                url: constants.apiServerOrigin + url,
                method: 'PUT',
                headers: angular.merge(_headers.authorization, _headers.contentType, headers),
                data: data
            }

            $http(request).then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function httpDelete(url, headers, options) {
            var deferred = $q.defer();

            var request = {
                url: constants.apiServerOrigin + url,
                method: 'DELETE',
                headers: angular.merge(_headers.authorization, _headers.contentType, headers)
            }

            $http(request).then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function getDefaultHeaders() {
            var authorization = {
                'Authorization': 'Basic =saASFe982dq19Dskadj'
            }
            var contentType = {
                'Content-Type': 'application/json'
            }
            return {
                authorization: authorization,
                contentType: contentType
            }
        }
    }
}());