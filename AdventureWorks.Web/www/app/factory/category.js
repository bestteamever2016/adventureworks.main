(function() {
    'use strict';

    angular.module('AdventureWorks.Factories').factory('category', ['$q', 'api', 'localStorageService', 'constants', category]);

    function category($q, api, storage, constants) {
        return {
            getCategories: getCategories,
            getSubCategories: getSubCategories
        }

        function getCategories() {
            var deferred = $q.defer();
            if (!storage.get(constants.lsKeys.productCategories)) {
                api.get('api/productcategory').then(function(response) {
                    storage.set(constants.lsKeys.productCategories, response.data);
                    deferred.resolve(response.data)
                }, function(error) {
                    deferred.reject(error);
                });
            } else {
                deferred.resolve(storage.get(constants.lsKeys.productCategories));
            }
            return deferred.promise;
        }


        function getSubCategories(categoryId) {
            var deferred = $q.defer();
            if (!storage.get(constants.lsKeys.productSubCategories + categoryId)) {
                api.get('api/productsubcategory?categoryId=' + categoryId).then(function(response) {
                    storage.set(constants.lsKeys.productSubCategories + categoryId, response.data);
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
            } else {
                deferred.resolve(storage.get(constants.lsKeys.productSubCategories + categoryId));
            }
            return deferred.promise;
        }
    }
}());