angular.module('AdventureWorks').config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
        .state('layout', {
            abstract: true,
            templateUrl: '/app/component/layout/layout.html',
            controller: 'LayoutController',
            controllerAs: 'ctrl'
        })
        .state('layout.home', {
            url: '/',
            templateUrl: '/app/component/home/home.html',
            controller: 'HomeController',
            controllerAs: 'ctrl',
            data: { pageTitle: 'Products' }
        })
        .state('layout.product', {
            url: '/product',
            templateUrl: '/app/component/product/product.html',
            controller: 'ProductController',
            controllerAs: 'ctrl',
            data: { pageTitle: 'Products' }
        })
        .state('layout.productCreate', {
            url: '/product/create',
            templateUrl: '/app/component/product/manage.html',
            controller: 'ProductCreateController',
            controllerAs: 'ctrl',
            data: { pageTitle: 'Products' }
        })
        .state('layout.productUpdate', {
            url: '/product/update/:id',
            templateUrl: '/app/component/product/manage.html',
            controller: 'ProductUpdateController',
            controllerAs: 'ctrl',
            data: { pageTitle: 'Products' }
        });

    $urlRouterProvider.otherwise('/');

    // $locationProvider.html5Mode({
    //     enabled: true,
    //     requireBase: false
    // });
}]);