angular.module('AdventureWorks').config(['localStorageServiceProvider',
function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('aw');
}]);