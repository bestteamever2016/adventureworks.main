﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orbus.AdventureWorks.DataAccess;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var ctx = new AdventureWorksContext("Data Source=.\\SQLEXPRESS;Initial Catalog=AdventureWorksDW2008R2;Integrated Security=True");
            var cat = ctx.ProductCategoryRepository.GetProductCategories();

            Console.ReadKey();
        }
    }
}
